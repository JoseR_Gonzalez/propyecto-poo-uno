<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' =>['auth']], function(){

    Route::get('/lista/productos',['as' => 'list.productos', 'uses' => 'productoscontroller@Iniciarproductos']);
    Route::get('/crear/productos',['as' => 'crear.productos', 'uses' => 'productoscontroller@CrearProducto']);
    Route::post('/guardar/productos',['as' => 'guardar.productos', 'uses' =>'ProductosController@GuardarProducto']);

    Route::get('/lista/Cliente',['as' => 'list.cliente', 'uses' => 'Clientecontroller@IniciarClientes']);
    Route::get('/crear/Cliente',['as' => 'crear.cliente', 'uses' => 'Clientecontroller@CrearCliente']);
    Route::post('/guardar/Cliente',['as' => 'guardar.cliente', 'uses' =>'ClienteController@GuardarCliente']);

});

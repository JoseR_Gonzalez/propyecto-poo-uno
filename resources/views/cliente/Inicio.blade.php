@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">MODULO DE CLIENTES</div>

                <div class ="col text-right">

                    <a href="{{route('crear.cliente')}}" class="btn btn-sm btn-primary">Nuevo Cliente</a>

                    </div>


                <div class="card-body">
                   
                <table class="table">
  <thead>
    <tr>
      <th scope="col">ID </th>
      <th scope="col">Nombre</th>
      <th scope="col">Apellidos</th>
      <th scope="col">Cedula</th>
      <th scope="col">Direccion</th>
      <th scope="col">Telefono</th>
      <th scope="col">Fecha de nacimiento</th>
      <th scope="col">Email</th>
    </tr>
  </thead>
  <tbody>
        @foreach ($clientes as $item)

    <tr>
      <th scope="row">{{$item->id}}</th>
      <td>{{$item->Nombre}}</td>
      <td>{{$item->Apellidos}}</td>
      <td>{{$item->Cedula}}</td>
      <td>{{$item->Direccion}}</td>
      <td>{{$item->Telefono}}</td>
      <td>{{$item->Fecha_nacimiento}}</td>
      <td>{{$item->Email}}</td>
    </tr>
    @endforeach
  </tbody>
</table>




                </div>
            </div>
        </div>
    </div>
</div>
@endsection
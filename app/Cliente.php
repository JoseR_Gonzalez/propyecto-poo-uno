<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    protected $fillable = [
        'ID',
        'Nombre',
        'Apellidos',
        'Cedula',
        'Direccion',
        'Telefono',
        'Fecha_nacimiento',
        'Email'
    ];
}

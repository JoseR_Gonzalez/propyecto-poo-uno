<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Cliente;

class Clientecontroller extends Controller
{
    public function IniciarClientes(Request $request)
    {
        
       $clientes = Cliente::all();
       return view ('cliente.Inicio')->with('clientes',$clientes);
    }

    public function CrearCliente(Request $request)
    {
        $clientes = Cliente::all();
        return view('cliente.crear')->with('Clientes', $clientes);
    }


    public function GuardarCliente(Request $request){
        $this->validate($request, [
            'Nombre' => 'required',
            'Apellidos'=> 'required',
            'Cedula'=> 'required',
            'Direccion'=> 'required',
            'Telefono'=> 'required',
            'Fecha_nacimiento'=> 'required',
            'Email'=> 'required'
        ]);

        $clientes = new Cliente;
        $clientes->Nombre=$request->Nombre;
        $clientes->Apellidos=$request->Apellidos;
        $clientes->Cedula=$request->Cedula;
        $clientes->Direccion=$request->Direccion;
        $clientes->Telefono=$request->Telefono;
        $clientes->Fecha_nacimiento=$request->Fecha_nacimiento;
        $clientes->Email=$request->Email;
        $clientes->save();
        return redirect()->route('list.cliente');
    }
}

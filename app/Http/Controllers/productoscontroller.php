<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Productos;

class productoscontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Iniciarproductos(Request $request)
    {
       $Productos = Productos::all();
       return view ('productos.Inicio')->with('Productos',$Productos);
    }


    public function CrearProducto(Request $request)
    {
        $Productos = Productos::all();
        return view ('productos.crear')->with('Productos',$Productos);

    }

    public function GuardarProducto(Request $request)
    {
        $this->validate($request, [
            'Nombre' => 'required',
            'Tipo'=> 'required',
            'Estado'=> 'required',
            'Precio'=> 'required'
        ]);

        $Productos = new Productos;
        $Productos->Nombre=$request->Nombre;
        $Productos->Tipo=$request->Tipo;
        $Productos->Estado=$request->Estado;
        $Productos->Precio=$request->Precio;
        $Productos->save();
        return redirect()->route('list.productos');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
